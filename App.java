import java.util.Scanner;

public class App{
    int a, b;
    void input(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first number: ");
        this.a = sc.nextInt();
        System.out.println("Enter second number: ");
        this.b = sc.nextInt();
    }
    int add(int a, int b){
        return a+b;
    }
    int sub(int a, int b){
        return a-b;
    }
    int mul(int a, int b){
        return a*b;
    }
    int div(int a, int b){
        return a/b;
    }
}